package main

import (
	"context"
	"fmt"
	"log"
	"time"

	pb "gitlab.com/DaidojiMayura/mstdn/pb"
	"google.golang.org/grpc"
)

func main() {
	conn, err := grpc.Dial("127.0.0.1:19003", grpc.WithInsecure())
	if err != nil {
		log.Fatal("client connection error:", err)
	}
	defer conn.Close()
	client := pb.NewStatusClient(conn)
	request := &pb.PostStatusRequest{Message: time.Now().String()}
	res, err := client.PostStatus(context.TODO(), request)
	fmt.Printf("result:%#v \n", res)
	fmt.Printf("error:%#v \n", err)
}
