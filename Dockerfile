FROM golang:latest

ADD . /go/src/gitlab.com/DaidojiMayura/mstdn

RUN go get google.golang.org/grpc && go get github.com/mattn/go-mastodon
RUN go install gitlab.com/DaidojiMayura/mstdn/cmd/server

ENTRYPOINT /go/bin/server

EXPOSE 19003