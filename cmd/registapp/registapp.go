package main

import (
	"context"
	"fmt"
	"log"

	"github.com/mattn/go-mastodon"
)

func main() {
	app, err := mastodon.RegisterApp(context.Background(), &mastodon.AppConfig{
		Server:     "https://mstdn.jp",
		ClientName: "md-mstdn-client",
		Scopes:     "read write follow",
		Website:    "https://gitlab.com/DaidojiMayura/mstdn",
	})
	if err != nil {
		log.Fatal(err)
	}
	fmt.Printf("client-id		: %s\n", app.ClientID)
	fmt.Printf("client-secret	: %s\n", app.ClientSecret)
}
