// Code generated by protoc-gen-go. DO NOT EDIT.
// source: status.proto

package status

import proto "github.com/golang/protobuf/proto"
import fmt "fmt"
import math "math"

import (
	context "golang.org/x/net/context"
	grpc "google.golang.org/grpc"
)

// Reference imports to suppress errors if they are not otherwise used.
var _ = proto.Marshal
var _ = fmt.Errorf
var _ = math.Inf

// This is a compile-time assertion to ensure that this generated file
// is compatible with the proto package it is being compiled against.
// A compilation error at this line likely means your copy of the
// proto package needs to be updated.
const _ = proto.ProtoPackageIsVersion2 // please upgrade the proto package

type PostStatusRequest struct {
	Message              string   `protobuf:"bytes,1,opt,name=message,proto3" json:"message,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *PostStatusRequest) Reset()         { *m = PostStatusRequest{} }
func (m *PostStatusRequest) String() string { return proto.CompactTextString(m) }
func (*PostStatusRequest) ProtoMessage()    {}
func (*PostStatusRequest) Descriptor() ([]byte, []int) {
	return fileDescriptor_status_b4883fc5915205ab, []int{0}
}
func (m *PostStatusRequest) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_PostStatusRequest.Unmarshal(m, b)
}
func (m *PostStatusRequest) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_PostStatusRequest.Marshal(b, m, deterministic)
}
func (dst *PostStatusRequest) XXX_Merge(src proto.Message) {
	xxx_messageInfo_PostStatusRequest.Merge(dst, src)
}
func (m *PostStatusRequest) XXX_Size() int {
	return xxx_messageInfo_PostStatusRequest.Size(m)
}
func (m *PostStatusRequest) XXX_DiscardUnknown() {
	xxx_messageInfo_PostStatusRequest.DiscardUnknown(m)
}

var xxx_messageInfo_PostStatusRequest proto.InternalMessageInfo

func (m *PostStatusRequest) GetMessage() string {
	if m != nil {
		return m.Message
	}
	return ""
}

type PostStatusResponse struct {
	Message              string   `protobuf:"bytes,1,opt,name=message,proto3" json:"message,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *PostStatusResponse) Reset()         { *m = PostStatusResponse{} }
func (m *PostStatusResponse) String() string { return proto.CompactTextString(m) }
func (*PostStatusResponse) ProtoMessage()    {}
func (*PostStatusResponse) Descriptor() ([]byte, []int) {
	return fileDescriptor_status_b4883fc5915205ab, []int{1}
}
func (m *PostStatusResponse) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_PostStatusResponse.Unmarshal(m, b)
}
func (m *PostStatusResponse) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_PostStatusResponse.Marshal(b, m, deterministic)
}
func (dst *PostStatusResponse) XXX_Merge(src proto.Message) {
	xxx_messageInfo_PostStatusResponse.Merge(dst, src)
}
func (m *PostStatusResponse) XXX_Size() int {
	return xxx_messageInfo_PostStatusResponse.Size(m)
}
func (m *PostStatusResponse) XXX_DiscardUnknown() {
	xxx_messageInfo_PostStatusResponse.DiscardUnknown(m)
}

var xxx_messageInfo_PostStatusResponse proto.InternalMessageInfo

func (m *PostStatusResponse) GetMessage() string {
	if m != nil {
		return m.Message
	}
	return ""
}

func init() {
	proto.RegisterType((*PostStatusRequest)(nil), "PostStatusRequest")
	proto.RegisterType((*PostStatusResponse)(nil), "PostStatusResponse")
}

// Reference imports to suppress errors if they are not otherwise used.
var _ context.Context
var _ grpc.ClientConn

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
const _ = grpc.SupportPackageIsVersion4

// StatusClient is the client API for Status service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://godoc.org/google.golang.org/grpc#ClientConn.NewStream.
type StatusClient interface {
	PostStatus(ctx context.Context, in *PostStatusRequest, opts ...grpc.CallOption) (*PostStatusResponse, error)
}

type statusClient struct {
	cc *grpc.ClientConn
}

func NewStatusClient(cc *grpc.ClientConn) StatusClient {
	return &statusClient{cc}
}

func (c *statusClient) PostStatus(ctx context.Context, in *PostStatusRequest, opts ...grpc.CallOption) (*PostStatusResponse, error) {
	out := new(PostStatusResponse)
	err := c.cc.Invoke(ctx, "/Status/PostStatus", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// StatusServer is the server API for Status service.
type StatusServer interface {
	PostStatus(context.Context, *PostStatusRequest) (*PostStatusResponse, error)
}

func RegisterStatusServer(s *grpc.Server, srv StatusServer) {
	s.RegisterService(&_Status_serviceDesc, srv)
}

func _Status_PostStatus_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(PostStatusRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(StatusServer).PostStatus(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/Status/PostStatus",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(StatusServer).PostStatus(ctx, req.(*PostStatusRequest))
	}
	return interceptor(ctx, in, info, handler)
}

var _Status_serviceDesc = grpc.ServiceDesc{
	ServiceName: "Status",
	HandlerType: (*StatusServer)(nil),
	Methods: []grpc.MethodDesc{
		{
			MethodName: "PostStatus",
			Handler:    _Status_PostStatus_Handler,
		},
	},
	Streams:  []grpc.StreamDesc{},
	Metadata: "status.proto",
}

func init() { proto.RegisterFile("status.proto", fileDescriptor_status_b4883fc5915205ab) }

var fileDescriptor_status_b4883fc5915205ab = []byte{
	// 120 bytes of a gzipped FileDescriptorProto
	0x1f, 0x8b, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0xff, 0xe2, 0xe2, 0x29, 0x2e, 0x49, 0x2c,
	0x29, 0x2d, 0xd6, 0x2b, 0x28, 0xca, 0x2f, 0xc9, 0x57, 0xd2, 0xe5, 0x12, 0x0c, 0xc8, 0x2f, 0x2e,
	0x09, 0x06, 0x8b, 0x05, 0xa5, 0x16, 0x96, 0xa6, 0x16, 0x97, 0x08, 0x49, 0x70, 0xb1, 0xe7, 0xa6,
	0x16, 0x17, 0x27, 0xa6, 0xa7, 0x4a, 0x30, 0x2a, 0x30, 0x6a, 0x70, 0x06, 0xc1, 0xb8, 0x4a, 0x7a,
	0x5c, 0x42, 0xc8, 0xca, 0x8b, 0x0b, 0xf2, 0xf3, 0x8a, 0x53, 0x71, 0xab, 0x37, 0x72, 0xe4, 0x62,
	0x83, 0xa8, 0x15, 0x32, 0xe7, 0xe2, 0x42, 0xe8, 0x14, 0x12, 0xd2, 0xc3, 0xb0, 0x55, 0x4a, 0x58,
	0x0f, 0xd3, 0x68, 0x25, 0x86, 0x24, 0x36, 0xb0, 0x43, 0x8d, 0x01, 0x01, 0x00, 0x00, 0xff, 0xff,
	0xf0, 0x9d, 0x71, 0x73, 0xb8, 0x00, 0x00, 0x00,
}
