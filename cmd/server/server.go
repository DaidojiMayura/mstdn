package main

import (
	"log"
	"net"

	pb "gitlab.com/DaidojiMayura/mstdn/pb"
	"gitlab.com/DaidojiMayura/mstdn/service"
	"google.golang.org/grpc"
)

func main() {
	listenPort, err := net.Listen("tcp", ":19003")
	if err != nil {
		log.Fatalln(err)
	}

	server := grpc.NewServer()
	statusService := &service.StatusService{}
	pb.RegisterStatusServer(server, statusService)
	server.Serve(listenPort)
}
