package service

import (
	"context"
	"os"

	pb "gitlab.com/DaidojiMayura/mstdn/pb"
	"github.com/mattn/go-mastodon"
)

// StatusService is struct
type StatusService struct {
}

// PostStatus is function
func (s *StatusService) PostStatus(ctx context.Context, request *pb.PostStatusRequest) (*pb.PostStatusResponse, error) {
	config := mastodon.Config{
		Server:       os.Getenv("MSTDN_SERVER"),
		ClientID:     os.Getenv("MSTDN_CLIENT_ID"),
		ClientSecret: os.Getenv("MSTDN_CLIENT_SECRET"),
		AccessToken:  os.Getenv("MSTDN_ACCESS_TOKEN"),
	}

	c := mastodon.NewClient(&config)

	_, err := c.PostStatus(context.Background(), &mastodon.Toot{
		Status:     request.Message,
		Visibility: "private",
	})
	if err != nil {
		return nil, err
	}

	return &pb.PostStatusResponse{
		Message: "",
	}, nil
}
